
// Cojo el id cell del html
// document.getElementByClassName("celda1")
// Le añado el evento y digo en la funcion que se hace
// .addEventListener("dblclick" , changeValue);

// Creo una funcion
// function changeValue(){
// En el id cell cambio el contenido por X
// document.getElementByClassName("celda1").innerHTML = "X";
// }

var cells = document.getElementsByClassName("celda");

[].forEach.call(cells, function (el) {
  el.addEventListener("click", whenClick);
  el.addEventListener("dblclick", whenDoubleClick);
});

function whenClick() {
  this.innerHTML = "X";
}

function whenDoubleClick() {
  this.innerHTML = "O";
}
